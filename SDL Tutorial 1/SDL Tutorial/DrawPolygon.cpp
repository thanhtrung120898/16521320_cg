#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = 3.1415;
    int canh = 3;
    double gocquay = 2*pi/canh;
    double gocphi = pi/2;
    int x[3];
    int y[3];
    for (int i = 0;i<3;i++)
    {
        x[i] = xc + int(R*cos(gocphi)+0.5);
        y[i] = yc - int(R*sin(gocphi)+0.5);
        gocphi = gocphi + gocquay;
    }
    for (int i=0;i<3;i++)
    {
        Bresenham_Line(x[i], y[i], x[(i+1)%3], y[(i+1)%3], ren);
    }

   
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = M_PI;
    double gocphi = pi/4;
    int x[4];
    int y[4];
    x[0] = int(cos(gocphi)*0 - (-R)*sin(gocphi)+0.5);
    y[0] = int(sin(gocphi)*0 + (-R)*cos(gocphi)+0.5);
    for (int i = 1;i<4;i++)
    {
        x[i] = int(cos(2*gocphi)*x[i-1] - y[i-1]*sin(2*gocphi)+0.5);
        y[i] = int(sin(2*gocphi)*x[i-1] + y[i-1]*cos(2*gocphi)+0.5);
        
    }

        Bresenham_Line(xc+x[0], yc+y[0], xc+x[1], yc+y[1], ren);
        Bresenham_Line(xc+x[0], yc+y[0], xc+x[3], yc+y[3], ren);
        Bresenham_Line(xc+x[1], yc+y[1], xc+x[2], yc+y[2], ren);
        Bresenham_Line(xc+x[2], yc+y[2], xc+x[3], yc+y[3], ren);
    
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = M_PI;
    double gocquay = 2*pi/5;
    double gocphi = pi/10;
    int x[5];
    int y[5];
    for (int i = 0;i<5;i++)
    {
        x[i] = xc + int(R*cos(gocphi)+0.5);
        y[i] = yc - int(R*sin(gocphi)+0.5);
        gocphi = gocphi + gocquay;
    }
    Bresenham_Line(xc+x[0], yc+y[0], xc+x[1], yc+y[1], ren);
    Bresenham_Line(xc+x[0], yc+y[0], xc+x[4], yc+y[4], ren);
    Bresenham_Line(xc+x[1], yc+y[1], xc+x[2], yc+y[2], ren);
    Bresenham_Line(xc+x[2], yc+y[2], xc+x[3], yc+y[3], ren);
    Bresenham_Line(xc+x[3], yc+y[3], xc+x[4], yc+y[4], ren);
    
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = M_PI;
    int canh = 6;
    double gocquay = 2*pi/6;
    double gocphi = pi/6;
    int x[6];
    int y[6];
    for (int i = 0;i<6;i++)
    {
        x[i] = xc + int(R*cos(gocphi)+0.5);
        y[i] = yc - int(R*sin(gocphi)+0.5);
        gocphi = gocphi + gocquay;
    }
    Bresenham_Line(xc+x[0], yc+y[0], xc+x[1], yc+y[1], ren);
    Bresenham_Line(xc+x[0], yc+y[0], xc+x[5], yc+y[5], ren);
    Bresenham_Line(xc+x[1], yc+y[1], xc+x[2], yc+y[2], ren);
    Bresenham_Line(xc+x[2], yc+y[2], xc+x[3], yc+y[3], ren);
    Bresenham_Line(xc+x[3], yc+y[3], xc+x[4], yc+y[4], ren);
    Bresenham_Line(xc+x[4], yc+y[4], xc+x[5], yc+y[5], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = M_PI;
    double gocquay = 2*pi/5;
    double gocphi = pi/2;
    int x[5];
    int y[5];
    for (int i = 0;i<5;i++)
    {
        x[i] = xc + int(R*cos(gocphi)+0.5);
        y[i] = yc - int(R*sin(gocphi)+0.5);
        gocphi = gocphi + gocquay;
    }
    Bresenham_Line(xc+x[0], yc+y[0], xc+x[2], yc+y[2], ren);
    Bresenham_Line(xc+x[0], yc+y[0], xc+x[3], yc+y[3], ren);
    Bresenham_Line(xc+x[1], yc+y[1], xc+x[3], yc+y[3], ren);
    Bresenham_Line(xc+x[1], yc+y[1], xc+x[4], yc+y[4], ren);
    Bresenham_Line(xc+x[2], yc+y[2], xc+x[4], yc+y[4], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = M_PI;
    double gocquay = 2*pi/5;
    double gocphi = pi/2;
    double rnho = ((sin(pi/10)*R)/(sin(7*pi/10)));
    double gocphinho =gocphi + pi/5;
    int x[5];
    int y[5];
    int x1[5];
    int y1[5];
    for (int i = 0;i<5;i++)
    {
            x[i] = xc + int(R*cos(gocphi)+0.5);
            y[i] = yc - int(R*sin(gocphi)+0.5);
            gocphi = gocphi + gocquay;
    }
    for (int i=0;i<5;i++)
        {
            x1[i] = xc + int(rnho*cos(gocphinho)+0.5);
            y1[i] = yc - int(rnho*sin(gocphinho)+0.5);
            gocphinho = gocphinho + gocquay;
        }
    for (int i=0;i<5;i++)
    {
        Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
        Bresenham_Line(x1[i], y1[i], x[(i+1)%5], y[(i+1)%5], ren);
    }


}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
    double pi = M_PI;
    double gocquay = 2*pi/8;
    double gocphi = pi/2;
    double rnho = ((sin(pi/8)*R)/(sin(3*pi/4)));
    double gocphinho =gocphi + pi/8;
    int x[8];
    int y[8];
    int x1[8];
    int y1[8];
    for (int i = 0;i<8;i++)
    {
        x[i] = xc + int(R*cos(gocphi)+0.5);
        y[i] = yc - int(R*sin(gocphi)+0.5);
        gocphi = gocphi + gocquay;
    }
    for (int i=0;i<8;i++)
    {
        x1[i] = xc + int(rnho*cos(gocphinho)+0.5);
        y1[i] = yc - int(rnho*sin(gocphinho)+0.5);
        gocphinho = gocphinho + gocquay;
    }
    for (int i =0 ;i<8;i++)
        {
            Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
            Bresenham_Line(x1[i], y1[i], x[(i+1)%8], y[(i+1)%8], ren);
        }
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
    double pi = M_PI;
    double gocquay = 2*pi/5;
    float gocphi = startAngle;
    double rnho = sin(pi/10)*R/sin(7*pi/10);
    double gocphinho =gocphi + pi/5;
    int x[5];
    int y[5];
    int x1[5];
    int y1[5];
    for (int i = 0;i<5;i++)
    {
        x[i] = xc + int(R*cos(gocphi)+0.5);
        y[i] = yc - int(R*sin(gocphi)+0.5);
        gocphi = gocphi + gocquay;
    }
    for (int i=0;i<5;i++)
    {
        x1[i] = xc + int(rnho*cos(gocphinho)+0.5);
        y1[i] = yc - int(rnho*sin(gocphinho)+0.5);
        gocphinho = gocphinho + gocquay;
    }
    for (int i=0;i<5;i++)
        {
            Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
            Bresenham_Line(x1[i], y1[i], x[(i+1)%5], y[(i+1)%5], ren);
        }
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
    float gocphi = M_PI / 2;
    while (r > 0)
    {
        DrawStarAngle(xc, yc, r, gocphi, ren);
        r = sin(M_PI / 10)*r / sin(7 * M_PI / 10);
        gocphi = gocphi + M_PI;
    }

}
