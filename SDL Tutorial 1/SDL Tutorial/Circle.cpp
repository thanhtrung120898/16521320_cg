#include "Circle.h"
#include <SDL2/SDL.h>

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;
    
    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc + y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    
    new_x = xc + y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc + x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc - x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc - y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc - y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    
    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    //7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
    int p = 3 - 2 * R;
    int x = 0;
    int y = R;
    Draw8Points(xc, yc, x, y, ren);
    while (x <= y)
    {
        if (p >= 0)
        {
            p = p + 4 * (x - y) + 10;
            Draw8Points(xc, yc, x++, y--, ren);
        }
        else
        {
            p = p + 4 * x + 6;
            Draw8Points(xc, yc, x++, y, ren);
        }
        
    }
    
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
    int p = 1 - R;
    int x = 0;
    int y = R;
    Draw8Points(xc, yc, x, y, ren);
    while (x < y)
    {
        if (p >= 0)
        {
            p = p + 2 * (x - y) + 5;
            y--;
        }
        else
        {
            p = p + 2 * x + 3;
        }
        x++;
        Draw8Points(xc, yc, x, y, ren);
    }
}


