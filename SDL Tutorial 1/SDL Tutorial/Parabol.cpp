#include "Parabol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
    int new_x;
    int new_y;
    
    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    
    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
    int x,y;
    int p = 1 - A;
    x=0;
    y=0;
    Draw2Points(xc, yc, x, y, ren);
    while (x <= A)
    {
        if (p <= 0) {
            p += 2 * x +3;
        }
        else {
            p += 2 *x+3 -2*A;
            y = y + 1;
        }
        x = x + 1;
       Draw2Points(xc, yc, x, y, ren);
    }
    
    x=A;
    y = double(A)/2;
    p = 2*A-1;
    while (xc + x <= 800 && xc+x>=0 && yc+y <= 600  && xc - x <=800 &&  xc-x>=0 )
    {
        if (p > 0)
        {
            p += 4*A - 2*(2*x+1)-2;
            x = x + 1;
           
        }
        else {
            p += 4*A;
        }
        y = y + 1;
        Draw2Points(xc, yc, x, y, ren);
    }

    

}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
    A = - A;
    double x,y;
    double p;
    x=0;
    y=0;
    p = 2*x + 1 + A;
    Draw2Points(xc, yc, x, y, ren);
    while (x <= -A)
    {
        if (p <= 0) {
            p =p + (2 * x + 3);
        }
        else {
            p =p + (2 *x + 3) + 2*A;
            y = y - 1;
        }
        x = x + 1;
        Draw2Points(xc, yc, x, y, ren);
    }
 
    x=  - A;
    y = double( A)/2;
    p = -1 - 2*A;
    Draw2Points(xc, yc, x, y, ren);

    while (xc + x <= 800 && xc+x>=0 && yc+y <= 600  && xc - x <=800 &&  xc-x>=0 )
    {
        if (p > 0)
        {
            p =p - 4*A - 2*(2*x+1)-2;
            x = x + 1;

        }
        else {
            p =p - 4*A;
        }
        y = y - 1;
        Draw2Points(xc, yc, x, y, ren);
    }

    
}
