#include <iostream>
#include <SDL2/SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE
    
    
    Vector2D v1(100,200);
    Vector2D v2(200,300);
    Vector2D v3(300,200);

    SDL_Color fillColor;
    fillColor.r = 0;
    fillColor.g = 255;
    fillColor.b = 0;
    fillColor.a = 255;
    

    //TriangleFill(v1, v2, v3, ren,fillColor);
    
    Vector2D p1(100,100);
    Vector2D p2(200,800);
    Vector2D p3(600,600);
    Vector2D p4(700,700);
    SDL_SetRenderDrawColor(ren, 255, 255, 255, 0);

    //DrawCurve2(ren, p1, p2, p3);
     DrawCurve3(ren, p1, p2, p3, p4);
    RectangleFill(p1, p2, ren, fillColor);
//    RectangleFill(p1, p2, ren, fillColor);
    
//    CircleFill(300, 300, 100, ren, fillColor);
//    CircleFill(200, 200, 200, ren, fillColor);
    

//    SDL_SetRenderDrawColor(ren, 255, 255, 255, 0);
//    FillIntersectionRectangleCircle(p1, p2, 200, 200, 200, ren, fillColor);
    
    //FillIntersectionEllipseCircle(250, 400, 200, 300, 400, 400, 200, ren, fillColor);
    //FillIntersectionTwoCircles(300, 300, 100, 200, 200, 200, ren, fillColor);
    SDL_RenderPresent(ren);
    //Take a quick break after all that hard work
    //Quit if happen QUIT event
    SDL_RenderPresent;
	bool running = true;


    while (running)
    {
        //If there's events to handle
        if (SDL_PollEvent(&event))
        {

            //If the user has Xed out the window
            if (event.type == SDL_QUIT)
            {
                //Quit the program
                running = false;
            }
            if (event.type == SDL_MOUSEMOTION)
            {
                double px[4] ={p1.x,p2.x,p3.x,p4.x};
                double py[4] = {p1.y,p2.y,p3.y,p4.y};
                int btn = 0;
                int posX = event.button.x;
                int posY = event.button.y;
                int Rcircle1 = (int)sqrt((posX - px[0])*(posX - px[0]) + (posY - py[0])*(posY - py[0])+0.5);
                int Rcircle2 = (int)sqrt((posX - px[1])*(posX - px[1]) + (posY - py[1])*(posY - py[1])+0.5);
                int Rcircle3 = (int)sqrt((posX - px[2])*(posX - px[2]) + (posY - py[2])*(posY - py[2])+0.5);
                int Rcircle4 = (int)sqrt((posX - px[3])*(posX - px[3]) + (posY - py[3])*(posY - py[3])+0.5);
                
                if (Rcircle1<=10)
                {
                    btn =1;
                }
                else if (Rcircle2<=10)
                {
                    btn =2;
                }
                else if (Rcircle3<=10)
                {
                    btn =3;
                }
                else if (Rcircle4<=10)
                {
                    btn =4;
                }

                    if (event.motion.state)
                    {
                        if (btn == 1)
                        {
                            SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
                            SDL_RenderClear(ren);
                            p1.x = event.button.x;
                            p1.y = event.button.y;
                            SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
                            //bac2
                            DrawCurve2(ren, p1, p2, p3);
                            //bac3
                            //DrawCurve3(ren, p1, p2, p3,p4);
                            SDL_RenderPresent(ren);
                            
                        }
                            else if (btn == 2) {
                            SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
                            SDL_RenderClear(ren);
                            p2.x = event.button.x;
                            p2.y = event.button.y;
                            SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
                            DrawCurve2(ren, p1, p2, p3);
                            //DrawCurve3(ren, p1, p2, p3,p4);
                            SDL_RenderPresent(ren);
                            
                        }
                            else if (btn == 3) {
                            SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
                            SDL_RenderClear(ren);
                            p3.x = event.button.x;
                            p3.y = event.button.y;
                            SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
                            DrawCurve2(ren, p1, p2, p3);
                            //DrawCurve3(ren, p1, p2, p3,p4);
                            SDL_RenderPresent(ren);
                        
                        }
                            else if (btn == 4) {
                            SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
                            SDL_RenderClear(ren);
                            p4.x = event.button.x;
                            p4.y = event.button.y;
                            SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
                            DrawCurve2(ren, p1, p2, p3);
                            //DrawCurve3(ren, p1, p2, p3,p4);
                            SDL_RenderPresent(ren);
                           
                        }
                    
                }
            }

        }
    }
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
